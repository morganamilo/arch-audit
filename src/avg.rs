use crate::enums::{Severity, Status};

#[derive(Clone, Debug)]
pub struct Avg {
    pub issues: Vec<String>,
    pub fixed: Option<String>,
    pub severity: Severity,
    pub status: Status,
    pub required_by: Vec<String>,
    pub avg_types: Vec<String>,
}

impl Default for Avg {
    fn default() -> Avg {
        Avg {
            issues: vec![],
            fixed: None,
            severity: Severity::Unknown,
            status: Status::Unknown,
            required_by: vec![],
            avg_types: vec![],
        }
    }
}
